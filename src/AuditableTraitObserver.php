<?php

namespace Fabriordonez\Auditable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class AuditableTraitObserver
{
    use DefaultAuditableId;

    /**
     * Model's creating event hook.
     *
     * @param Model $model
     */
    public function creating(Model $model)
    {
        $createdBy = $model->getCreatedByColumn();
        $updatedBy = $model->getUpdatedByColumn();

        if (!$model->$createdBy) {
            $model->$createdBy = $this->getAuthenticatedUserId();
        }

        if (!$model->$updatedBy) {
            $model->$updatedBy = $this->getAuthenticatedUserId();
        }
    }

    /**
     * Get authenticated user id depending on model's auth guard.
     *
     * @return int
     */
    protected function getAuthenticatedUserId()
    {
        return auth()->check() ? auth()->id() : $this->getDefaultAuditableId();
    }

    /**
     * Model's updating event hook.
     *
     * @param Model $model
     */
    public function updating(Model $model)
    {
        $updatedBy = $model->getUpdatedByColumn();

        if (!$model->isDirty($updatedBy)) {
            $model->$updatedBy = $this->getAuthenticatedUserId();
        }
    }
}
